package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void testSquareCompletion() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(2, 2, 2);

        // Draw lines to complete a square
        grid.drawLine(0, 0, 0, 1); // vertical
        grid.drawLine(0, 1, 1, 1); // horizontal
        grid.drawLine(1, 0, 1, 1); // vertical
        boolean completed = grid.drawLine(0, 0, 1, 0); // horizontal, should complete the square

        assertTrue(completed, "Square should be completed");
        assertEquals(1, grid.getBoxOwner(0, 0), "Player 1 should own the square");
    }

    @Test
    public void testDrawingExistingLine() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(2, 2, 2);

        // Draw a line
        grid.drawLine(0, 0, 0, 1);

        // Draw the same line again
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawLine(0, 0, 0, 1);
        });

        assertEquals("This vertical line is already drawn.", exception.getMessage());
    }
}
