package dotsandboxes;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DotsAndBoxesUI {

    final DotsAndBoxesGrid grid;
    final JLabel label;
    final JPanel anchorPane;

    final int lineThickness = 4;
    final int dotDiameter = 12;
    final int cellSize = 40;

    public DotsAndBoxesUI(DotsAndBoxesGrid grid) {
        this.grid = grid;

        anchorPane = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                paintGrid(g);
            }
        };
        anchorPane.setPreferredSize(new Dimension(cellSize * grid.width, cellSize * grid.height));

        label = new JLabel();
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setPreferredSize(new Dimension(cellSize * grid.width, 30));

        // Add a watcher to update the UI when the grid changes
        grid.addWatcher((g) -> {
            updateStatus();
            anchorPane.repaint();
        });

        anchorPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();

                int gridX = x / cellSize;
                int gridY = y / cellSize;

                if (x % cellSize < lineThickness && gridX < grid.width && gridY < grid.height - 1) {
                    // Vertical line
                    try {
                        grid.drawLine(gridX, gridY, gridX, gridY + 1);
                    } catch (IllegalStateException ex) {
                        JOptionPane.showMessageDialog(anchorPane, "This line is already drawn!");
                    }
                } else if (y % cellSize < lineThickness && gridY < grid.height && gridX < grid.width - 1) {
                    // Horizontal line
                    try {
                        grid.drawLine(gridX, gridY, gridX + 1, gridY);
                    } catch (IllegalStateException ex) {
                        JOptionPane.showMessageDialog(anchorPane, "This line is already drawn!");
                    }
                }
            }
        });

        updateStatus();
    }

    private void updateStatus() {
        label.setText("Current player: " + grid.getPlayer());
    }

    private void paintGrid(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);

        for (int x = 0; x < grid.width; x++) {
            for (int y = 0; y < grid.height; y++) {
                int xPos = x * cellSize;
                int yPos = y * cellSize;
                g2d.fillOval(xPos - dotDiameter / 2, yPos - dotDiameter / 2, dotDiameter, dotDiameter);

                if (x < grid.width - 1 && grid.isHorizontalLineDrawn(x, y)) {
                    g2d.fillRect(xPos + dotDiameter / 2, yPos - lineThickness / 2, cellSize - dotDiameter, lineThickness);
                }

                if (y < grid.height - 1 && grid.isVerticalLineDrawn(x, y)) {
                    g2d.fillRect(xPos - lineThickness / 2, yPos + dotDiameter / 2, lineThickness, cellSize - dotDiameter);
                }

                if (x < grid.width - 1 && y < grid.height - 1 && grid.getBoxOwner(x, y) != 0) {
                    g2d.setColor(grid.getBoxOwner(x, y) == 1 ? Color.RED : Color.BLUE);
                    g2d.fillRect(xPos + dotDiameter / 2, yPos + dotDiameter / 2, cellSize - dotDiameter, cellSize - dotDiameter);
                    g2d.setColor(Color.BLACK);
                }
            }
        }
    }
}
