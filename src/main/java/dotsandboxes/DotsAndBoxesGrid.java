package dotsandboxes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;

/**
 * The state of a dots and boxes grid.
 *
 * A (4, 3) dots and boxes grid looks like this:
 *
 * *-*-*-*
 * | | | |
 * *-*-*-*
 * | | | |
 * *-*-*-*
 *
 * Notice that:
 *
 * - for each row, there is one less horizontal than the number of corner dots
 * - for each row, there are as many verticals as there are corner dots
 * - for each row, there is one less box than the number of corner dots
 * - for each column, there is one less vertical than the number of corner dots.
 * - for each column, there are as many horizontals as there are corner dots.
 * - for each column, there is one less box than the number of corner dots
 *
 * For example, in this (4, 3) grid, there are (3, 3) horizontal lines, and (4, 2) vertical lines, and (3, 2) boxes.
 *
 * We number all lines and boxes by their top-left coordinate.
 *
 * In Java 14+, we might use a Record class for this, but we're using 11+ as an LTS version, so we don't have that yet.
 */
public class DotsAndBoxesGrid {

    final int width;
    final int height;

    /** The horizontal lines in the grid. True if drawn. */
    private boolean[][] horizontals;

    /** The vertical lines in the grid. True if drawn. */
    private boolean[][] verticals;

    /** Which owner (if any) claimed any given box. */
    private int[][] boxOwners;

    /** A list of functions to notify when there is an update */
    private ArrayList<Consumer<DotsAndBoxesGrid>> watchers = new ArrayList<>();

    final int players;
    private int player = 1;
    public int getPlayer() {
        return player;
    }

    /** Moves to the next player */
    private void nextPlayer() {
        player++;
        if (player > players) {
            player = 1;
        }
    }

    public DotsAndBoxesGrid(int width, int height, int players) {
        this.width = width;
        this.height = height;
        this.players = players;
        this.horizontals = new boolean[width - 1][height];
        this.verticals = new boolean[width][height - 1];
        this.boxOwners = new int[width - 1][height - 1];
    }

    public void addWatcher(Consumer<DotsAndBoxesGrid> watcher) {
        this.watchers.add(watcher);
    }

    private void notifyWatchers() {
        for (Consumer<DotsAndBoxesGrid> watcher : watchers) {
            watcher.accept(this);
        }
    }

    public boolean drawLine(int x1, int y1, int x2, int y2) {
        if (x1 == x2 && Math.abs(y1 - y2) == 1) {
            if (verticals[x1][Math.min(y1, y2)]) {
                throw new IllegalStateException("This vertical line is already drawn.");
            }
            verticals[x1][Math.min(y1, y2)] = true;
        } else if (y1 == y2 && Math.abs(x1 - x2) == 1) {
            if (horizontals[Math.min(x1, x2)][y1]) {
                throw new IllegalStateException("This horizontal line is already drawn.");
            }
            horizontals[Math.min(x1, x2)][y1] = true;
        } else {
            throw new IllegalArgumentException("Lines must be vertical or horizontal and of length 1.");
        }

        boolean completedBox = false;

        // Check for any completed boxes
        for (int x = 0; x < width - 1; x++) {
            for (int y = 0; y < height - 1; y++) {
                if (boxOwners[x][y] == 0 &&
                        horizontals[x][y] &&
                        horizontals[x][y + 1] &&
                        verticals[x][y] &&
                        verticals[x + 1][y]) {
                    boxOwners[x][y] = player;
                    completedBox = true;
                }
            }
        }

        if (!completedBox) {
            nextPlayer();
        }

        notifyWatchers();
        return completedBox;
    }

    public int getBoxOwner(int x, int y) {
        return boxOwners[x][y];
    }

    public boolean isHorizontalLineDrawn(int x, int y) {
        return horizontals[x][y];
    }

    public boolean isVerticalLineDrawn(int x, int y) {
        return verticals[x][y];
    }
}
